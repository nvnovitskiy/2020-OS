# Операционные системы (Самарский Государственный Технический Университет)
____

# Лабораторные работы:

 № Л/р | Название | Статус| Ссылка
 ----- |----------|-------|------
 1 | Знакомство с операционной системой Linux | ✅| [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter1)
 2 | Введение в курс программирования под Linux| ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter2)
 3 | Процессы в операционной системе Linux | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter3)
 4 | Процессы в операционной системе Linux. Реализация генеалогического дерева процессов | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter4)
 5 | Организация взаимодействия процессов в ОС Linux. Работа с pipe и FIFO | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter5)
 6 | Организация взаимодействия процессов в ОС Linux. Взаимодействие процессов через FIFO | ❌ | ❌ 
 7 | Организация работы с разделяемой памятью в ОС Linux | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter7)
 8 | Синхронизации процессов с помощью семафоров | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter8) 
 9 | Синхронизации процессов с помощью сообщений | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter9)  
10 | Организация файловой системы в Linux. Работа с файлами и директориями. Символьные ссылки | ❌ | ❌ 
11 | Организация файловой системы в Linux. Анализ содержимого директории | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter11)  
12 | Организация файловой системы в Linux. Memory mapped файлы | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter12)
13 | Аппарат прерываний. Работа с сигналами. | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/chapter13)
14 | Курсовой проект | ✅ | [Ссылка](https://github.com/nvnovitskiy/2020-OS/tree/main/course%20project)  
