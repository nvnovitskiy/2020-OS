#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
int main(void)
{
    char map[] = "mapped.dat";
    int fd, *knl, j;
    size_t size, n = 1000, sum = 0, summ = 0;
    fd = open(map, O_RDWR | O_CREAT, 0666);
    if (fd == -1){
        printf("Can't open file %s!\n", map);
        exit(1);
    }
    size = n * 4 * sizeof(int);
    ftruncate(fd, size);
    knl = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
    close(fd);
    if (knl == MAP_FAILED){
        printf("Mapping failed!\n");
        exit(2);
    }
    for(j = 0; j < n; j++){
        sum += j + 1;
        summ += (j + 1) * (j + 1);
        knl[4 * j] = j + 1;
        knl[4 * j + 1] = (j + 1) * (j + 1);
        knl[4 * j + 2] = sum;
        knl[4 * j + 3] = summ;
    }
    munmap((void*)knl, size);
    return 0;
}
