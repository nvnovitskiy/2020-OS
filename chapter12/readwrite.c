#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
int main(void)
{
    char map[] = "mapped.dat";
    char txt[] = "result.txt";
    FILE* file;
    int fd, *knl, j, n = 1000;
    size_t size;
    fd = open(map, O_RDONLY, 0666);
    if (fd == -1){
        printf("Can't open file %s!\n", map);
        exit(1);
    }
    size = 4 * n * sizeof(int);
    knl = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
    close(fd);
    if (knl == MAP_FAILED){
        printf("Mapping failed!\n");
        exit(2);
    }
    file = fopen(txt, "w");
    if (file == NULL){
        printf("Can't open file %s!\n", txt);
        exit(1);
    }
    for(j = 0; j < n; j++){
        fprintf(file, "%11d%11d%11d%11d\n", knl[4*j], knl[4*j + 1], knl[4*j + 2], knl[4*j + 3]);
    }
    fclose(file);
    munmap((void*)knl, size);
    return 0;
}


