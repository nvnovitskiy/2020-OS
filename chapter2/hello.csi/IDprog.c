/* ID.c */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
void print_ID (void)
{
	printf("Индефикатор пользователя %d\n", getuid());
	printf("Индефикатор группы %d\n", getgid());
}
