/* ID.C */
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
int print_ID()
{
pid_t pid, ppid;
int a = 0;
(void)fork();
a=a+1;
pid=getpid();
ppid=getppid();
printf("My pid = %d, result = %d\n", (int)pid, (int)ppid, a);
return 0;
}
