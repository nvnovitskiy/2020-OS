#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
int main()
{
	int a = 0;
	pid_t pid = fork();
	if(pid==0)
	{
	a=a+1;
	printf("a=%d\n",a);
	exit(1);
	}
	if(pid>0)
	{
	a=a+2;
	printf("a=%d\n",a);
	}
	printf("Я тут\n");
	return 0;
}
