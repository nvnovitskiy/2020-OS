#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void blue(pid_t pid, pid_t ppid)
{
        printf("Blue: Я родился!\n");
        printf("Blue: Мой PID = %d, мой PPID = %d!\n", pid, ppid);
        printf("Blue: Я закончил выполнение!\n");
}
