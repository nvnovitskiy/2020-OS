#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void main()
{
        pid_t pid_yellow;
        int yellow_exit_code;

        switch(pid_yellow=fork()) {
        case -1:
                perror("Yellow Error"); /* произошла ошибка */
                exit(1); /*выход из родительского процесса*/
        case 0:
                yellow(getpid(), getppid());
            exit(yellow_exit_code);
        default:
          printf("Green: Я процесс-родитель!\n");
          printf("Green: Мой PID = %d\n", getpid());
          printf("Green: PID моего потомка Yellow = %d\n", pid_yellow);
          printf("Green: Я жду, пока все потомки не вызовут exit()...\n");
          wait(&yellow_exit_code);
          printf("Green: Yellow отработал и завершился с кодом %d\n", WEXITSTATUS(yellow_exit_code));
          printf("Green: Я закончил работу!\n");
	}
}
