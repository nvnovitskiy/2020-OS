#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void orange(pid_t pid, pid_t ppid)
{
        printf("Orange: Я родился!\n");
        printf("Orange: Мой PID = %d, мой PPID = %d!\n", pid, ppid);
        printf("Orange: Я закончил выполнение!\n");
}
