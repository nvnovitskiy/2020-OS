#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void purple(pid_t pid, pid_t ppid)
{
        printf("Purple: Я родился!\n");
        printf("Purple: Мой PID = %d, мой PPID = %d!\n", pid, ppid);
        printf("Purple: Мой PID = %d, мой PPID = %d! (перепроверка)\n", getpid(), getppid());
        printf("Purple: Готовлюсь стать родителем!\n");

        pid_t pid_orange;
        int orange_exit_code;

        switch(pid_orange=fork()) {
        case -1:
                perror("Orange Error"); /* произошла ошибка */
                exit(1); /*выход из родительского процесса*/
        case 0:
                orange(getpid(), getppid());
            exit(orange_exit_code);
        default:
          printf("Purple: Я теперь тоже процесс-родитель!\n");
          printf("Purple: Мой PID = %d\n", getpid());
          printf("Purple: PID моего потомка Orange = %d\n", pid_orange);
          printf("Purple: Я жду, пока все потомки не вызовут exit()...\n");
          wait(&orange_exit_code);
          printf("Purple: Orange завершился с кодом  %d\n", WEXITSTATUS(orange_exit_code));
          printf("Purple: Я закончил работу!\n");
        }

}
