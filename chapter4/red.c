#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void red(pid_t pid, pid_t ppid)
{
        printf("Red: Я родился!\n");
        printf("Red: Мой PID = %d, мой PPID = %d!\n", pid, ppid);
        printf("Red: Мой PID = %d, мой PPID = %d! (перепроверка)\n", getpid(), getppid());
        printf("Red: Готовлюсь стать родителем!\n");

        pid_t pid_blue;
        int blue_exit_code;

        switch(pid_blue=fork()) {
        case -1:
                perror("Blue Error"); /* произошла ошибка */
                exit(1); /*выход из родительского процесса*/
        case 0:
                blue(getpid(), getppid());
            exit(blue_exit_code);
        default:
          printf("Red: Я теперь тоже процесс-родитель!\n");
          printf("Red: Мой PID = %d\n", getpid());
          printf("Red: PID моего потомка Blue = %d\n", pid_blue);
          printf("Red: Я жду, пока все потомки не вызовут exit()...\n");
          wait(&blue_exit_code);
          printf("RED: Blue завершился с кодом  %d\n", WEXITSTATUS(blue_exit_code));
          printf("RED: Я закончил работу!\n");
        }

}
