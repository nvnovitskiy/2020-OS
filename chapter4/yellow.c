#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void yellow(pid_t pid, pid_t ppid)
{
        printf("Yellow: Я родился!\n");
        printf("Yellow: Мой PID = %d, мой PPID = %d!\n", pid, ppid);
        printf("Yellow: Мой PID = %d, мой PPID = %d! (перепроверка)\n", getpid(), getppid());
        printf("Yellow: Готовлюсь стать родителем!\n");

        pid_t pid_purple, pid_red;
        int purple_exit_code, red_exit_code;

        switch(pid_purple=fork()) {
        case -1:
                perror("Purple Error"); /* произошла ошибка */
                exit(1); /*выход из родительского процесса*/
        case 0:
                purple(getpid(), getppid());
            exit(purple_exit_code);
        default:

        switch(pid_red=fork()) {
        case -1:
                perror("Red Error"); /* произошла ошибка */
                exit(1); /*выход из родительского процесса*/
        case 0:
                red(getpid(), getppid());
            exit(red_exit_code);
        default:

          printf("Yellow: Я теперь тоже процесс-родитель!\n");
          printf("Yellow: Мой PID = %d\n", getpid());
          printf("Yellow: PID моего потомка Purple = %d\n", pid_purple);
          printf("Yellow: PID моего потомка Red = %d\n", pid_red);
          printf("Yellow: Я жду, пока все потомки не вызовут exit()...\n");
          wait(&purple_exit_code);
          wait(&red_exit_code);
          printf("Yellow: Purple завершился с кодом  %d\n", WEXITSTATUS(purple_exit_code));
          printf("Yellow: Red завершился с кодом  %d\n", WEXITSTATUS(red_exit_code));
          printf("Yellow: Я закончил работу!\n");
        }
	}
}
