/* openfile.c */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
int openfile (char* path, int flags)
{
        int fd;
        fd  = open(path, flags);
        if (fd <0)
        {
                printf("Can\'t open file\n");
                exit(-1);
        }
        return fd;
}
