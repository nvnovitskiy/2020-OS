#include <fcntl.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
int main()
{
        char string[31];
        size_t size;
        ssize_t ssize;
        int fd;
        fd  = open("myfile_mod",O_RDONLY);
        if (fd <0)
        {
                printf("Can\'t open file\n");
                exit(-1);
        }
        size = sizeof(string);
        ssize=read(fd,string,size);
        if ((int)size !=(int)ssize)
        {
                printf("Error of read\n");
                exit(-1);
        }
        printf("%s\n",string);
        if (close(fd)<0) {printf("Cannot close file\n");}
        return 0;
}
