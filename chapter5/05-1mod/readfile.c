/* readfile.c */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
void readfile (int descriptor, char* maswrite, size_t count)
{
        ssize_t rebytes = read(descriptor, maswrite, count);
        if ((int)count !=(int)rebytes)
        {
                printf("Error of read\n");
                exit(-1);
        }
}
