#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
int main(){
	int fd, result;
	size_t size;
	char resstring[30];
	char name[]="aaa.fifo";
	if(mknod(name, S_IFIFO | 0666, 0) < 0){
	printf("Can't create FIFO\n");
	exit(-1);
	}
	if((result = fork()) < 0){
	printf("Can't fork child\n");
	exit(-1);
	} else if (result > 0) {
	if((fd  = open(name, O_WRONLY)) < 0){
        printf("Can't open FIFO for writing\n");
        exit(-1);
	}
	size = write(fd, "Novitsky Nikita Vladimirovich!", 30);
        if(size != 30){
                printf("Can't write all string to FIFO\n");
                exit(-1);
        }
	close(fd);
	wait();
	close(fd);
	}
	return 0;
}
