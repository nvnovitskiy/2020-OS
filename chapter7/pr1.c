#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
int main()
{
	char *fd = "myfile.txt";
	struct stat buf;
	stat(fd, &buf);
	int size = buf.st_size;
	char *str;
	str = (char*) malloc(size);
	int shmid;
	char pathname[]="pr1.c";
	key_t key;
	//разделяемая прамять
	if ((key = ftok(pathname,0))<0)
	{
	printf("Can\'t generate key\n");
	exit(-1);
	}
	if((shmid = shmget(key, size*sizeof(int), 0666|IPC_CREAT|IPC_EXCL))<0)
	{
	if(errno != EEXIST)
	{
	printf("Can\'t create shared memory\n");
	exit(-1);
	} else
	{
	if ((shmid = shmget(key, size*sizeof(int),0))<0)
	{
	printf("Can\'t find shared memory\n");
	exit(-1);
	}
	}
	}
	if((str=(char *)shmat(shmid,NULL,0))== (char *)(-1))
	{
	printf("Can't attach shared memory\n");
	exit(-1);
	}
	//чтение из файла
	FILE *myfile;
	myfile=fopen ("myfile.txt","r");
	char arr [100];
	while(fgets(arr,100, myfile)!=NULL)strcpy( str,arr);
	if (shmdt(str)<0)
	{
	printf("Can't detach shared memory. Error=%d\n", errno);
	exit(-1);
	}
	return 0;
}
