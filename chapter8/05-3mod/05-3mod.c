#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <sys/ipc.h>
int main()
{
	int semid;
	key_t key;
	struct sembuf mybuf;
	char pathname[] = "05-3mod.c";
        int fd1[2], fd2[2], result;
        size_t size;
        char string[]="Novitsky Nikita Vladimirovich";
        char string1[]="3-IAIT-10";
        char resstring1[sizeof(string)], resstring2[sizeof(string1)];
	if((key = ftok(pathname, 0)) < 0)
	{
	printf("Can\'t generate key\n");
	exit(-1);
	}
	if((semid = semget(key, 1, 0666 | IPC_CREAT)) < 0)
	{
	printf("Can\'t get semid\n");
	exit(-1);
	}
        if(pipe(fd1) < 0)
	{
        printf("Can't create pipe\n");
        exit(-1);
        }
        if(pipe(fd2) < 0)
	{
        printf("Can't create pipe2\n");
        exit(-1);
        }
        result = fork();
        if(result == -1)
	{
                printf("Can't fork child\n");
                exit(-1);
        }
	else if(result > 0)
	{
		mybuf.sem_op = 1;
		mybuf.sem_flg = 0;
		mybuf.sem_num = 0;
		if(semop(semid, &mybuf, 1) < 0)
		{
		printf("Can\'t wait for condition\n");
		exit(-1);
		}
                close(fd1[0]);
                close(fd2[1]);
                size = write(fd1[1], string, sizeof(string));
       		if(size !=sizeof(string))
	{
                printf("Can\'t write all string\n");
                exit(-1);
        }
                close(fd1[1]);
                size = read(fd1[0],resstring1, sizeof(string));
                printf("%s\n",string);
                close(fd2[0]);
                }
		else
		{	mybuf.sem_op = 1;
			mybuf.sem_flg = 0;
			mybuf.sem_num = 0;
			if(semop(semid, &mybuf, 1) < 0)
			{
			printf("Can\'t wait for condition\n");
			exit(-1);
			}
                        close(fd1[1]);
                        close(fd2[0]);
                        size = write(fd1[0], string1, sizeof(string1));
                        if(size < 0)
			{
                                printf("Can't read string\n");
                                exit(-1);
                        }
                        size = read(fd2[1],resstring2, sizeof(string1));
                        printf("%s\n",string1);
                        close(fd1[0]);
                        close(fd2[1]);

        }
        wait();
        return 0;
}
