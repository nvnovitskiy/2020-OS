#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <string.h>
int main(int argc, char *argv[])
{
        key_t key;
        int semid, shmid;
        struct sembuf sops;
        char *shmaddr;
        char str[256];
        key=12345;
        if ((shmid = shmget(key, 256, IPC_CREAT | 0666)) < 0) { perror("shmget"); return 1; }
        if ((shmaddr = (char*)shmat(shmid, NULL, 0)) == (void*)-1) { perror("shmat"); return 1; }
        semid = semget(key, 1, IPC_CREAT | 0666);
        semctl(semid, 0, IPC_SET, 0);
        sops.sem_num = 0;
        sops.sem_flg = 0;
        do {
                // Вводим входную строку
                do {
                        printf("Введите строку(Введите X для завершения):");
                } while (fgets(str, 255, stdin) == NULL);
                // Копируем данные в общую память
                strcpy(shmaddr, str);
                printf("Строка скопирована в общий буфер\n");
                // Освобождаем доступ
                sops.sem_op = 1;
                semop(semid, &sops, 1);
                printf("Доступ разрешен для общего буфера\n");
        } while (str[0] != 'X');
        shmdt(shmaddr);
        semctl(semid,0,IPC_RMID, 0);
        shmctl(shmid,IPC_RMID,NULL);
        return 0;
}
