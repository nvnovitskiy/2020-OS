#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define LAST_MESSAGE 255
struct date {
    int day;
    char month[20];
    int year;
};
struct persone {
    char firstname[20];
    char lastname[20];
    struct date bd;
};
struct mymsgbuf {
    long mtype;
    struct persone pers;
};
int main(void) {
    int msqid;
    char pathname[] = "09-1a_mod.c";
    key_t key;
    int i,len;
    struct mymsgbuf mybuf;
    if ((key = ftok(pathname, 0)) < 0) {
        printf("Can't generate key\n");
        exit(1);
    }
    if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0) {
        printf("Can\'t get msqid\n");
        exit(1);
    }
    printf("Enter info about 5 persons\n");
    for (i = 1; i <= 5; i++) {
        mybuf.mtype = 1;
        printf("Info about person %d\n", i);
        printf("First name: ");
        scanf("%s", mybuf.pers.firstname);
        printf("Last name: ");
        scanf("%s", mybuf.pers.lastname);
        printf("BithDate (format: dd month yyyy): ");
        scanf("%d%s%d", &mybuf.pers.bd.day, mybuf.pers.bd.month, &mybuf.pers.bd.year);
        printf("\n");
        len = sizeof(mybuf.pers);
        if (msgsnd(msqid, (struct msgbuf *) &mybuf, len, 0) < 0) {
            printf("Can\'t send message to queue\n");
            msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
            exit(1);
        }
    }
    mybuf.mtype = LAST_MESSAGE;
    len = 0;
    if (msgsnd(msqid, (struct msgbuf *) &mybuf, len, 0) < 0) {
        printf("Can\'t send message to queue\n");
        msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
        exit(1);
    }
    return 0;
}
