#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define LAST_MESSAGE 255
struct date {
    int day;
    char month[20];
    int year;
};
struct persone {
    char firstname[20];
    char lastname[20];
    struct date bd;
};
struct mymsgbuf {
    long mtype;
    struct persone pers;
};
int main()
{
    int msqid;
    char pathname[] = "09-1a_mod.c";
    key_t key;
    int len, maxlen;
    struct mymsgbuf mybuf;
    if((key = ftok(pathname,0)) < 0) {
        printf("Can\'t generate key\n");
        exit(1);
    }
    if((msqid = msgget(key, 0666 | IPC_CREAT)) < 0){
        printf("Can\'t get msqid\n");
        exit(1);
    }
    while(1) {
        maxlen = sizeof(mybuf.pers);
        if ((len = msgrcv(msqid, (struct msgbuf *) &mybuf, maxlen, 0, 0) < 0)) {
            printf("Can\'t receive message from queue\n");
            exit(1);
        }
        if (mybuf.mtype == LAST_MESSAGE) {
            msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
            exit(0);
        }
        printf("message type: %ld, name: %s %s, birth date: %d/%s/%d\n", 
            mybuf.mtype, mybuf.pers.firstname, mybuf.pers.lastname,
            mybuf.pers.bd.day, mybuf.pers.bd.month, mybuf.pers.bd.year);
    }
    return 0;
}
