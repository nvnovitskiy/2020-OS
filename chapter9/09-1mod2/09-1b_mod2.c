#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define PROC1 1
#define PROC2 2
struct mymsgbuf
{ 
    long mtype;
    char mtext[1000]; 
};

int main()
{
    int msqid;
    char pathname[] = "09-1a_mod2.c";
    key_t key;
    int i, len, maxlen;
    struct mymsgbuf mybuf;
    if ((key = ftok(pathname,0)) < 0) {
        printf("Can\'t generate key\n");
        exit(1);
    } 
    if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0){
        printf("Can\'t get msqid\n");
        exit(1);
    }
    for (i = 1; i <= 5; i++) {
        printf("Waiting message from prog1\n");
        maxlen = 81;
        if ((len = msgrcv(msqid, (struct msgbuf *) &mybuf, maxlen, 0, PROC1) < 0)) {
            printf("Can\'t receive message from queue\n");
            exit(1);
        }
        printf("Message from prog1: type = %ld, info = %s\n", 
            mybuf.mtype, mybuf.mtext);
        mybuf.mtype = PROC2;
        printf("Enter message to send: ");
        fgets(mybuf.mtext, 81, stdin);
        len = strlen(mybuf.mtext) + 1;
        if (len - 2 > 0 && mybuf.mtext[len - 2] == '\n') {
            mybuf.mtext[len - 2] = '\0';
            len--;
        }
        if (msgsnd(msqid, (struct msgbuf *) &mybuf, len, 0) < 0) {
            printf("Can\'t send message to queue\n");
            msgctl(msqid, IPC_RMID, (struct msqid_ds *) NULL);
            exit(1);
        }
    }
    return 0;
}
