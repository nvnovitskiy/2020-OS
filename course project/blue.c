#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

extern pid_t parent;

void blue(void)
{
	fprintf(stderr, "  BLUE: Begun pid=%d ppid=%d\n", getpid(), getppid());

    kill(parent, SIGUSR1);
    fprintf(stderr, "  BLUE: SIGUSR1 signal sent!\n");

	kill(parent, SIGUSR2);
    fprintf(stderr, "  BLUE: SIGUSR2 signal sent!\n");

	fprintf(stderr, "  BLUE: Ended pid=%d ppid=%d\n", getpid(), getppid());
	exit(0);
}
