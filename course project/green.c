#include <semaphore.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>

pid_t parent;
int fildes[2];
sem_t *sem, *done;
int *pnum;
int count1 = 0;
int count2 = 0;
const char *filename = "number.txt";

extern void yellow(void);

void my_handler1(int signum)
{
	if (signum == SIGUSR1)
	{
		count1 = count1 + 1;
		printf(" GREEN: Got signal SIGUSR1!:%d\n", count1);
		sleep(2);
	}
}

void my_handler2(int signum)
{
	if (signum == SIGUSR2)
	{
		count2 = count2 + 1;
		printf(" GREEN: Got signal SIGUSR2!:%d\n", count2);
		sleep(2);
	}
}

void green(void)
{
	pid_t pid_yellow;
	int yellow_exit_code;
	int num;

	fprintf(stderr, " GREEN: Begun pid=%d\n", getpid());

	fprintf(stderr, " GREEN: I wait until all descendant call exit()...\n");

	signal(SIGUSR1, my_handler1);
	signal(SIGUSR2, my_handler2);

	if ((pid_yellow = fork()) == -1)
	{
		fprintf(stderr, " GREEN: fork(yellow): %s\n", strerror(errno));
		exit(1);
	}

	else if (pid_yellow == 0)
	{
		yellow();
	}

	read(fildes[0], &num, sizeof(num));
	fprintf(stderr, " GREEN: read from pipe: %d\n", num);

	*pnum = num;
	sem_post(sem);
	fprintf(stderr, " GREEN: write to shared memory: %d\n", *pnum);

	waitpid(pid_yellow, NULL, 0);

	wait(&yellow_exit_code);

	fprintf(stderr, " GREEN: YELLOW worked and completed with code %d\n", WEXITSTATUS(yellow_exit_code));
	fprintf(stderr, " GREEN: Ended pid=%d\n", getpid());
}

int main(void)
{
	int shm;

	parent = getpid();

	unlink("fifo");
	if (mkfifo("fifo", 0600) == -1)
	{
		perror("mkfifo()");
		exit(1);
	}

	if (pipe(fildes) == -1)
	{
		perror("pipe()");
		exit(1);
	}

	if ((sem = sem_open("mysem", O_CREAT, 0600, 0)) == SEM_FAILED)
	{
		perror("sem_open(mysem)");
		exit(1);
	}
	if ((done = sem_open("mydone", O_CREAT, 0600, 0)) == SEM_FAILED)
	{
		perror("sem_open(mydone)");
		exit(1);
	}

	if ((shm = shm_open("myshm", O_CREAT|O_RDWR, 0600)) == -1)
	{
		perror("shm_open()");
		exit(1);
	}
	ftruncate(shm, sizeof(*pnum));
	pnum = mmap(NULL, sizeof(*pnum), PROT_READ|PROT_WRITE, MAP_SHARED, shm, 0);
	if (pnum == (int *)-1)
	{
		perror("mmap()");
		exit(1);
	}

	green();

	unlink("fifo");
	sem_unlink("mysem");
	sem_unlink("mydone");
	close(shm);
	shm_unlink("myshm");

	return 0;
}
