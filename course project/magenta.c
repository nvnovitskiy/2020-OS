#include <semaphore.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/wait.h>

extern pid_t parent;
extern sem_t *done;
extern const char *filename;
extern void orange(void);

void magenta(void)
{
    pid_t pid_orange;
    int orange_exit_code;
	char folder[32];
	char newname[32];
	time_t t;

	fprintf(stderr, "MAGENTA: Begun pid=%d ppid=%d\n", getpid(), getppid());
    fprintf(stderr, "MAGENTA: I wait until all descendant call exit()...\n");
	kill(parent, SIGUSR1);
    fprintf(stderr, "MAGENTA: SIGUSR1 signal sent!\n");

    if ((pid_orange = fork()) == -1)
	{
		fprintf(stderr, "MAGENTA: fork(orange): %s\n", strerror(errno));
		exit(1);
	}
	else if (pid_orange == 0)
	{
		orange();
	}

	sem_wait(done);
	t = time(NULL);
	strftime(folder, sizeof(folder), "%Y%m%d-%H%M%S", localtime(&t));
	strcpy(newname, folder);
	strcat(newname, "/");
	strcat(newname, filename);
	mkdir(folder, 0700);
	rename(filename, newname);
	symlink(newname, filename);
	fprintf(stderr, "MAGENTA: Creating folder: %s\n", folder);

    waitpid(pid_orange, NULL, 0);

    kill(parent, SIGUSR2);
    fprintf(stderr, "MAGENTA: SIGUSR2 signal sent!\n");

    wait(&orange_exit_code);
    fprintf(stderr, "MAGENTA: ORANGE worked and completed with code %d\n", WEXITSTATUS(orange_exit_code)); 
	fprintf(stderr, "MAGENTA: Ended pid=%d ppid=%d\n", getpid(), getppid());
	exit(0);
}
