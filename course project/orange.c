#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/wait.h>

extern pid_t parent;

void orange(void)
{
	int fd;
	char c;
	int nl = 1;
	int num = 0;

	fprintf(stderr, "ORANGE: Begun pid=%d ppid=%d\n", getpid(), getppid());

	kill(parent, SIGUSR1);
    fprintf(stderr, "ORANGE: SIGUSR1 signal sent!\n");

	sleep(1);
	while (read(0, &c, 1) == 1)
	{
		if (nl)
		{
			nl = 0;
			fprintf(stderr, "ORANGE: got: %d '", num + 1);
		}
		if (c == '\n')
		{
			nl = 1;
			num++;
			fprintf(stderr, "'");
		}
		fprintf(stderr, "%c", c);
	}

	if ((fd = open("fifo", O_WRONLY)) == -1)
	{
		fprintf(stderr, "ORANGE: open(fifo): %s\n", strerror(errno));
		exit(1);
	}
	write(fd, &num, sizeof(num));
	close(fd);
	fprintf(stderr, "ORANGE: write to fifo: %d\n", num);

	kill(parent, SIGUSR2);
	fprintf(stderr, "ORANGE: SIGUSR2 signal sent!\n");

	fprintf(stderr, "ORANGE: Ended pid=%d ppid=%d\n", getpid(), getppid());
	exit(0);
}
