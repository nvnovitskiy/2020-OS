#include <semaphore.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/wait.h>

extern pid_t parent;
extern sem_t *sem, *done;
extern int *pnum;
extern const char *filename;

extern void blue(void);


void red(void)
{
	pid_t pid_blue;
    int blue_exit_code;
	int num;
	int fd;
	char snum[32];

	fprintf(stderr, "   RED: Begun pid=%d ppid=%d\n", getpid(), getppid());

    fprintf(stderr, "   RED: I wait until all descendant call exit()...\n");

	kill(parent, SIGUSR1);
    fprintf(stderr, "   RED: SIGUSR1 signal sent!\n");

	if ((pid_blue = fork()) == -1)
	{
		fprintf(stderr, "   RED: fork(blue): %s\n", strerror(errno));
		exit(1);
	}
	else if (pid_blue == 0)
	{
		blue();
	}


	sem_wait(sem);
	num = *pnum;
	fprintf(stderr, "   RED: read from shared memory: %d\n", num);

	unlink(filename);
	if ((fd = open(filename, O_WRONLY|O_CREAT|O_TRUNC, 0600)) == -1)
	{
		fprintf(stderr, "   RED: open(): %s\n", strerror(errno));
		exit(1);
	}
	sprintf(snum, "%d\n", num);
	write(fd, snum, strlen(snum));
	close(fd);
	fprintf(stderr, "   RED: write to file: %s", snum);

	sem_post(done);

	waitpid(pid_blue, NULL, 0);

   	wait(&blue_exit_code);
    kill(parent, SIGUSR2);
    fprintf(stderr, "   RED: SIGUSR2 signal sent!\n");

    fprintf(stderr, "   RED: BLUE worked and completed with code %d\n", WEXITSTATUS(blue_exit_code));
	fprintf(stderr, "   RED: Ended pid=%d ppid=%d\n", getpid(), getppid());
    exit(0);
}
