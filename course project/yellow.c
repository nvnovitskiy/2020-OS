#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/wait.h>

extern pid_t parent;

extern void magenta(void);
extern void red(void);
extern int fildes[2];

void yellow(void)
{
	int fd;
	int num;
    pid_t pid_magenta, pid_red;
    int magenta_exit_code, red_exit_code;
	fprintf(stderr, "YELLOW: Begun pid=%d ppid=%d\n", getpid(), getppid());
    fprintf(stderr, "YELLOW: I wait until all descendant call exit()...\n");
	kill(parent, SIGUSR1);
    fprintf(stderr, "YELLOW: SIGUSR1 signal sent!\n");
    if((pid_magenta = fork()) == -1)
    {
        fprintf(stderr, "YELLOW: fork(magenta): %s\n", strerror(errno));
        exit(1);
    }
    else if(pid_magenta == 0)
    {
        magenta();
    }
    
    if((pid_red = fork()) == -1)
    {
        fprintf(stderr, "YELLOW: fork(red): %s\n", strerror(errno));
        exit(1);
    }
    else if(pid_red == 0)
    {
        red();
    }

    if ((fd = open("fifo", O_RDONLY)) == -1)
	{
		fprintf(stderr, "YELLOW: open(fifo): %s\n", strerror(errno));
		exit(1);
	}

    read(fd, &num, sizeof(num));
	close(fd);
	fprintf(stderr, "YELLOW: read from fifo: %d\n", num);

	num *= num;
	write(fildes[1], &num, sizeof(num));
	fprintf(stderr, "YELLOW: write to pipe: %d\n", num);
    
    waitpid(pid_magenta, NULL, 0);
    waitpid(pid_red, NULL, 0);    

    kill(parent, SIGUSR2);
    fprintf(stderr, "YELLOW: SIGUSR2 signal sent!\n");

    wait(&magenta_exit_code);
    wait(&red_exit_code);
    
    fprintf(stderr, "YELLOW: MAGENTA worked and completed with code %d\n", WEXITSTATUS(magenta_exit_code));
    fprintf(stderr, "YELLOW: RED worked and completed with code %d\n", WEXITSTATUS(red_exit_code));
	fprintf(stderr, "YELLOW: Ended pid=%d ppid=%d\n", getpid(), getppid());
	exit(0);
}
